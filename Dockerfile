FROM ixeny/angular-nginx:permissions

COPY . /workspace/
WORKDIR /workspace

RUN yarn install
RUN ng build

WORKDIR /usr/share/nginx/html
RUN cp -R /workspace/dist/front/* .
CMD nginx -g 'daemon off;'