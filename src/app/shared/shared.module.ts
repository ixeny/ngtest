import {NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  imports: [
    BrowserModule
  ],
  exports: [],
  declarations: [],
  providers: [],
})
export class SharedModule {
}
